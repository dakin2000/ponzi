from django.conf.urls import url
from .models import Profile
import views

urlpatterns = [
    url(r'^update/$', views.edit_profile_view, name="update"),
    url(r'^dashboard/$', views.admin_dashboard, name="admin_dashboard"),
    url(r'^downline/$', views.downline_tree_view, name="downline_tree_view"),
    url(r'^FAQ/$', views.faq, name="faq"),
    url(r'^steps/$', views.steps, name="steps"),
    url(r'^approve/(?P<profile_id>[0-9]+)/$', views.approve_view, name="approve"),
    url(r'^payment/$', views.payment_view, name="payment"),
	url(r'^profile/$', views.profile_view, name="profile"),
	url(r'^login/', views.login_view, name='login'),
	url(r'^logout/', views.logout_view, name='logout'),
    url(r'^users/', views.user_list, name='user_list'),
    url(r'^payment_list/', views.payment_list, name='payment_list'),
	url(r'^$', views.home, name="home"),
    
]
