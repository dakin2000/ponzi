# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-03-15 13:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Level',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(default=0)),
                ('amount', models.CharField(blank=True, default=0, max_length=15)),
                ('num_users', models.IntegerField(default=0)),
                ('approvals', models.IntegerField(default=0)),
                ('upgrade_ammount', models.CharField(blank=True, default=0, max_length=15)),
                ('status', models.BooleanField(default=False)),
            ],
        ),
    ]
