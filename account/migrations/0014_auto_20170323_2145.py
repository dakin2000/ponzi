# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-03-23 21:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0013_payment_payment_to'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='payment_to',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='paying', to='account.Profile'),
        ),
    ]
