# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-03-23 21:55
from __future__ import unicode_literals

import account.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0014_auto_20170323_2145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='payment_snapshot',
            field=models.ImageField(blank=True, null=True, upload_to=account.models.payment_upload_location),
        ),
    ]
