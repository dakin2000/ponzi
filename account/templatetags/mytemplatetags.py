from django import template
register = template.Library()

@register.filter(name='convert_to_naira')
def convert_to_naira(arg):
    """
    We store all amount in kobo by default
    This tt convers to Naira for web display, with formatting
    """
    float_amount = float(arg)
    return "{:20,.2f}".format(float_amount)