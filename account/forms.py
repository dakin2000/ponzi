
from .models import Profile,Payment

from django import forms
from django.contrib.auth import (
	authenticate,
	get_user_model,
	login,
	logout,
	)
from django.http import HttpResponseRedirect
from django.utils.translation import gettext_lazy as _


User = get_user_model()




class LoginForm(forms.Form):
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder':"username", 'name':"name"}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'type':'password',"placeholder":"Password",'class': 'form-control', "name":"password"}))
	
	def clean(self, *args, **kwargs):
		username = self.cleaned_data.get('username')
		password = self.cleaned_data.get('password')

		if username and password:
			user = authenticate(username=username, password=password)
			if not user:
				raise forms.ValidationError("Invalid username and password")

			if not user.check_password(password):
				raise forms.ValidationError("Wrong password")

			if not user.is_active:
				raise forms.ValidationError("This user is no longer active")

		return super(LoginForm, self).clean(*args, **kwargs)


class RegistrationForm(forms.ModelForm):
	GenderChoices = (
		('M', 'Male'),
		('F', 'Female')
		)
	username =  forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder':"username", 'name':"name"}))
	email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',"type":"email","placeholder":"Email", "name":"email"}))
	phone_number = forms.CharField(widget=forms.TextInput(attrs={'type':'text',"placeholder":"Phone",'class': 'form-control', "name":"phone_number"}))
	gender = forms.ChoiceField(widget=forms.RadioSelect(attrs={'name':"option-yes"}),choices=GenderChoices)
	password = forms.CharField(widget=forms.PasswordInput(attrs={'type':'password',"placeholder":"Password",'class': 'form-control', "name":"password"}))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs={'type':'password',"placeholder":"Confirm password",'class': 'form-control', "name":"password2"}))
	
	class Meta:
		model = User
		fields = [
			'username',
			'email',
			'gender',
			'phone_number',
			'password',
			'password2',

		]

	def clean_password2(self):
		password = self.cleaned_data.get('password')
		password2 = self.cleaned_data.get('password2')

		if password != password2:
			raise forms.ValidationError('Password must match')

		return password2



class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username','first_name','last_name','email']

class ProfileForm(forms.ModelForm):
	GenderChoices = (
		('M', 'Male'),
		('F', 'Female')
		)
	gender = forms.ChoiceField(widget=forms.RadioSelect,choices=GenderChoices)
	phone_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Mobile Number...','name':'mobile'}))
	address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Address','name':'Address'}))
	state = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'State','name':'State'}))
	city = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'City','name':'City'}))
	bank_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Bank','name':'bank_name'}))
	account_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Account Name','name':'account_name'}))
	account_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Account Number','name':'account_number'}))
	class Meta:
		model = Profile
		fields = ['gender','address','phone_number','state','city','bank_name','account_name','account_number']



# class ApprovalForm(forms.ModelForm):
# 	class Meta:
# 		model = Approval
# 		fields = ['user','status']

# class PaymentForm(forms.ModelForm):
# 	# payment_to = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Pay to','name':'payment_to'}))
# 	class Meta:
# 		model = Payment
# 		fields = ['payment_to','amount','bank','payment_snapshot','payment_means','date_paid']

	# def clean_payment_to(self):
	# 	payment_to = self.cleaned_data.get('payment_to')
	# 	user = self.cleaned_data.get('user')

	# 	payment = Payment.objects.filter(user=user)
	# 	for i in payment:
	# 		if i.payment_to == payment_to:
	# 			raise forms.ValidationError('sorry you have already paid this user.')
	# 			break
	# 	return payment

class PaymentForm(forms.Form):
	AMOUNT_CHOICE = (
		('500','N500'),
		('750','N750'),
		('2000','N2,000'),
		('10000','N10,000'),
		('80000','N80,000'),
		('500000','N500,000'),
		)
	PAYMENT_MEANS_CHOICE = (
		('B','Bank Deposit'),
		('A','ATM Transfer'),
		('M','Mobile Transfer'),
	)
	user = forms.CharField(label=_("I am"),max_length=255,widget=forms.TextInput,)
	payment_to = forms.CharField(label=_("Paying To"),max_length=255,widget=forms.TextInput,)
	amount = forms.ChoiceField(label=_("Amount"),choices=AMOUNT_CHOICE,)
	bank = forms.CharField(label=_("Bank"),max_length=255,widget=forms.TextInput,)
	payment_snapshot = forms.ImageField(label=_("Payment Snapshot"))
	payment_means = forms.ChoiceField(label=_("Payment Means"),choices=PAYMENT_MEANS_CHOICE,)

	def clean_payment_to(self):
		payment_to = self.cleaned_data['payment_to']
		user = self.cleaned_data['user']
		payments_queryset = Payment.objects.all()
		payment_user_profiles = [x.user.username for x in payments_queryset]
		if payment_to != 'bola':
			if user in payment_user_profiles:
				beneficiaries = [i.payment_to.user.username for i in payments_queryset.filter(user__username=user) ]
				if payment_to in beneficiaries:
					raise forms.ValidationError("Sorry you can only pay this user once. Please contact the admin for support")	
		return payment_to

class ApprovalForm(forms.ModelForm):
	class Meta:
		model = Payment
		fields = ['user','status']


# class EditProfileForm(forms.ModelForm):
# 	GenderChoices = (
# 		('M', 'Male'),
# 		('F', 'Female')
# 		)
# 	FirstName = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'FirstName','name':'firstname'}))
# 	LastName =  forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'LastName','name':'lastname'}))
# 	OtherName = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'OtherName','name':'othername'}))
# 	Gender = forms.ChoiceField(widget=forms.RadioSelect,choices=GenderChoices)
# 	PhoneNumber = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Mobile Number...','name':'mobile'}))
# 	Email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter your email...','name':'email'}))
# 	Address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Address','name':'Address'}))
# 	State = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'State','name':'State'}))
# 	City = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'City','name':'City'}))
# 	class Meta:
# 		model = Profile
# 		exclude = ['AccountType','IPAdress','Timestamp','user']
		# fields = "__all__" 