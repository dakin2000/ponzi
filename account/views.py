# Create your views here.
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponseRedirect,Http404
from django.conf import settings
from django.contrib.auth import (
	authenticate,
	get_user_model,
	login,
	logout,
	)

from django.shortcuts import render,redirect,get_object_or_404

from .forms import (
				ApprovalForm,
				LoginForm,
				RegistrationForm,
				ProfileForm,
				UserForm,
				PaymentForm,
				)
from .models import Profile,Payment
from .utils import *
from testimony.forms import TestimonyForm

# Create your views here.#18a689
def admin_dashboard(request):
	all_users = Profile.objects.all()
	all_payments = Payment.objects.all()
	admin_account = Payment.objects.filter(user__username='bola')
	available_users = all_users.filter(available=True)
	total_payment = cal_total_payments(all_payments)
	total_admin_payment = cal_total_payments(admin_account)
	context = {
		'users' : all_users,
		'payments' : all_payments,
		'available_users' : available_users,
		'total_payment' : total_payment,
		'total_admin_payment' : total_admin_payment,
	}
	return render(request,'admin_dashboard.html',context)

# Create your views here.#18a689
def home(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/profile')


	try:
		refer_id = request.session['ref']
		obj = Profile.objects.get(id=refer_id)
		print 'i am '+ str(obj)  + ' and my id is '+str(refer_id)
	except:
		obj = None

	next = request.GET.get('next')
	title = 'Register'
	form = RegistrationForm(request.POST or None)
	if form.is_valid():
		user = form.save(commit = False)
		password = form.cleaned_data.get("password")
		user.set_password(password)
		user.save()   #creating the user
		#getting the user profile and presetting ref_id and ip
		user_profile = Profile.objects.get(user=user)
		user_profile.ip_address = get_ip_address(request)
		user_profile.ref_id = get_ref_id()
		user_profile.phone_number = form.cleaned_data.get('phone_number')
		if not obj == None:
			user_profile.direct_referral = obj
		user_profile.save()

		#checkiing if user is among the first two of direct upline
		direct_upline = user_profile.direct_referral
		# print direct_upline
		# upline_downline = direct_upline.referral.all()
		auth_user = authenticate(username=user.username, password=password)
		login(request,auth_user)
		if next:
			return redirect(next)
		return redirect('/profile')

	context = {
		'registration_form' : form,
		'upline': obj,
	}
	return render(request,'index.html',context)




def login_view(request):
	print request.user.is_authenticated()
	next = request.GET.get('next')
	user_login_form = LoginForm(request.POST or None)
	if user_login_form.is_valid():
		print "the form is ok"
		username = user_login_form.cleaned_data.get('username')
		password = user_login_form.cleaned_data.get('password')
		user = authenticate(username=username, password=password)
		login(request, user)
		if next:
			return redirect(next)
		print request.user.is_authenticated()
		return redirect('/')


	context = {
		'form' : user_login_form,
		'title' : 'Login',
	}
	return render(request, 'form.html', context)


def approve_view(request,profile_id):
	#approve user
	user = Profile.objects.get(id=profile_id)
	form = ApprovalForm(request.POST or None,instance=user)
	# print form
	# print user,user.level_number
	if form.is_valid():
		form.save(commit=False)
		# updating the payment
		# payment = get_object_or_404(Payment,user__username=user)
		payment = Payment.objects.filter(user__username=user).last()
		if payment == None:
			raise Http404("Payment does not exist.Please ask your downline to upgrade by filling the payment form.")
		payment.status = True
		payment.save()
		form.save()
		# num_approved = Payment.objects.filter(payment_to=request.user.profile).count()
		user.level_number = int(user.level_number) + 1
		user.save()
		
		messages.success(request, ('Thanks for successfully approving your downline payment.'))
		return redirect('account:profile')
	context = {
		'form' : form,
	}
	return render(request,'approve_form.html',context)

def profile_view(request):
	if not request.user.is_authenticated:
		return redirect('/login')

	if request.user.is_superuser:
		return redirect('account:admin_dashboard')
	
	user = request.user.profile
	print type(user)
	p = [] #list of uplines
	p = pay_to(user,p)
	print p
	level = request.user.profile.level_number
	paying_to = None
	try:
		paying_to = p[level]
		# if paying_to.level_number==user.level_number:
		# 	paying_to = Profile.objects.get(user__username='bola')
	except:
		if not request.user.is_superuser:
			paying_to = p[0]
		else:
			paying_to = None	

	x = []
	title = 'Profile'
	user = Profile.objects.get(pk=request.user.profile.id)
	downline = get_downline(user,x)

	ref_url = settings.SHARE_URL + str(user.ref_id)

	#checkiing if user is among the first two of direct upline
	direct_upline = user.direct_referral
	upline_downline = direct_upline.referral.all()
	if user not in upline_downline[:2]:
		user.available=True
		user.save()

	if user.referral.all():
		user.available = False
		user.save()

	# spill over test
	go_ahead = False
	if request.user.profile.level_number == 0:
		if not request.user.profile in paying_to.referral.all()[:2]:
			user.direct_referral = Profile.objects.get(user__username='bola')
			user.available = True
			user.save()

	# determining of to display users payers
	payment_queryset = Payment.objects.filter(user=request.user)
	current_payment_status = False
	if payment_queryset.last():
		current_payment_status =  payment_queryset.last().status

	paid_me = Payment.objects.filter(payment_to__user__username=request.user.username).filter(status=True)
	paid_me = [str(i) for i in paid_me]

	# determining the number of approvals already made
	num_approved = Payment.objects.filter(payment_to=request.user.profile).filter(status=True).count()
	go_ahead_to_pay_status = False
	num_user_remaining = 0
	rate = [0,2,6,14,30,62,126]
	if num_approved == rate[level]:
		go_ahead_to_pay_status = True
	# number of users left to move you to the next lever
	num_user_remaining = rate[level] - num_approved
	# rendering and subitting testimonies
	testimony_form = TestimonyForm(request.POST or None)
	if testimony_form.is_valid():
		instance = testimony_form.save(commit=False)
		instance.author = request.user.profile
		testimony_form.save()
		print 'well done sir'

	context = {
		'title' : title,
		'user' : user,
		'ref_url':ref_url,
		'payers': downline,
		'paying_to' : paying_to,
		'payment' : current_payment_status,
		'num_approved':num_approved,
		'num_user_remaining' : num_user_remaining,
		'go_ahead_to_pay_status':go_ahead_to_pay_status,
		'paid_me':paid_me,
		'testimony_form' : testimony_form,
		
	}
	return render(request,'profile.html',context)

def faq(request):
	context = {

	}

	return render(request,'FAQ.html',context)

def steps(request):
	context = {

	}

	return render(request,'steps.html',context)

def downline_tree_view(request):
	dic = {}
	tree_list = []
	downline = get_downline_t(request.user.profile)
	tree_data = convert_to_dic(downline,dic)
	for i in tree_data.values():
		if len(i) >= 1:
			for j in i:
				tree_list.append(j)
		else:
			tree_list.append(i)
	simple_chart_config = [request.user.username] + tree_list
	print tree_list
	print simple_chart_config
	context = {
		'tree_data': tree_data,
		'downline': downline[:2],
		'simple_chart_config':simple_chart_config
	}
	return render(request,'downline_view.html',context)



def payment_view(request):
	user = request.user.profile
	print type(user)
	p = [] #list of uplines
	p = pay_to(user,p)
	print p
	level = request.user.profile.level_number
	paying_to = None
	try:
		paying_to = p[level]
	except:
		if not request.user.is_superuser:
			paying_to = p[0]
		else:
			paying_to = None	
	num_approved = Payment.objects.filter(payment_to=request.user.profile).filter(status=True).count()
	go_ahead_to_pay_status = False
	num_user_remaining = 0
	rate = [0,2,6,14,30,62,126]
	if num_approved == rate[level]:
		go_ahead_to_pay_status = True

	form = PaymentForm(initial={'user':request.user.username,'payment_to':paying_to.user.username})
	
	if request.method=='POST':
		form = PaymentForm(request.POST,request.FILES)
		if form.is_valid():
			paying_to=form.cleaned_data.get('payment_to')
			# print 'helo ',dir(paying_to)
			paying_to = Profile.objects.get(user__username=paying_to)
			amount = form.cleaned_data.get('amount')
			bank = form.cleaned_data.get('bank')
			payment_snapshot = form.cleaned_data.get('payment_snapshot')
			payment_means = form.cleaned_data.get('payment_means')
			# Creating the payment
			payment = Payment.objects.create(
				user = request.user,
				payment_to = paying_to,
				amount = amount,
				bank = bank,
				payment_snapshot = payment_snapshot,
				payment_means = payment_means,
				)
			messages.success(request, ('Your payment was successfully made. Please contact your upline to confirm your payment!'))
			return redirect('account:profile')
	context = {
		'form' : form,
		'paying_to' : paying_to,
		'go_ahead_to_pay_status':go_ahead_to_pay_status,
	}
	return render(request,'payment_form.html',context)




@login_required
@transaction.atomic
def edit_profile_view(request):
	title = 'Edit Profile'
	user = request.user.profile
	if request.method == 'POST':
		user_form = UserForm(request.POST or None, instance=request.user)
		profile_form = ProfileForm(request.POST or None, instance=request.user.profile)
		# form.actual_user = request.user
		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			messages.success(request, ('Your profile was successfully updated!'))
			return redirect('account:profile')
			# return HttpResponseRedirect(reverse('profile:profile_update'))
		else:
			messages.error(request, ('Please correct the error below.'))
	else:
		user_form = UserForm(instance=request.user)
		profile_form = ProfileForm(instance=request.user.profile)

	context = {
		'user_form' : user_form,
		'profile_form':profile_form,
		'title' : title,
		'user': user,
	}
	return render(request,'user_profile_update.html',context)

@login_required
@transaction.atomic
def logout_view(request):
	logout(request)
	
	return redirect('/')


def user_list(request):
	all_users = Profile.objects.all()

	context = {
		'users' : all_users,
	}
	return render(request,'users.html',context)

def payment_list(request):
	all_payments = Payment.objects.all()

	context = {
		'payments' : all_payments,
	}
	return render(request,'payment.html',context)