import collections
import datetime
import math
import re
import uuid

from account.models import Profile


def get_ip_address(request):
	ip_address = ''
	try:
		x_forward = request.META.get("HTTP_X_FORWARDED_FOR")
		if x_forward:
			ip_address = x_forward
		else:
			ip_address = request.META.get("REMOTE_ADDR")
	except:
		ip_address = ''

	return ip_address

def get_ref_id():
	ref_id = str(uuid.uuid4())[:10].replace('-','')
	try:
		ref_id_exist = Profile.objects.get(ref_id=ref_id)
		get_ref_id()
	except:
		return ref_id

def get_downline(user,total_downline=[],count=1):
    direct_refer = user.referral.all()[:2]
    level_refer = []
    for i in direct_refer:
        total_downline.append((i,count))
        if i.referral.all():
            get_downline(i,total_downline,count+1)
    for i in total_downline:
        if i[1] == int(user.level_number):
            level_refer.append(i[0])
    return level_refer




def pay_to(user_x,x_list):
    x = user_x.direct_referral
    if x:
        x_list.append(x)
        x = pay_to(x,x_list)
    return x_list


def cal_total_payments(payments):
    total = 0
    for i in payments:
        total += int(i.amount)
    return total


def get_downline_t(user):
    direct_refer = list(user.referral.all()[:2])
    if len(direct_refer)==1:
        direct_refer.append(None)
    level_refer = list(direct_refer)
    for i in direct_refer:
        if i == None:
            continue
        if i.referral.all():
            level_refer.append(get_downline_t(i))
    return level_refer


# second step
# def convert_to_dic(downline,dic):
#     count=0
#     if len(downline) > 2:
#         for i in downline[:2]:
#             dic[downline[count]] = downline[count+2][:2]
#             convert_to_dic(downline[count+2],dic)
#             count += 1
#     return dic
# 2


# def convert_to_dic(downline,dic):
#     count=0
#     if len(downline)<=2:
#         dic[downline[0]] = downline[0]
#     elif len(downline) > 2:
#         for i in downline[:2]:
#             dic[downline[count]] = downline[count+2][:2]
#             convert_to_dic(downline[count+2],dic)
#             count += 1
#     return dic

def convert_to_dic(downline,dic):
    count=0
    if len(downline) > 2:
        for i in downline[:2]:
            try:
                dic[downline[count]] = downline[count+2][:2]
                convert_to_dic(downline[count+2],dic)
                count += 1
            except:
                pass
            
    return dic