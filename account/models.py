from __future__ import unicode_literals

from django.utils import timezone

from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


def upload_location(instance,filename):
	return "{0}/{1}/ProfilePicture/{2}".format(instance.user,instance.FirstName,filename)

def payment_upload_location(instance,filename):
	return "{0}/{1}/{2}/{3}".format(instance.user,instance.payment_to,instance.payment_means,filename)

	

class Profile(models.Model):	
	user= models.OneToOneField(User,on_delete=models.CASCADE)
	gender = models.CharField(max_length=5, default='', null=True, blank=True)
	direct_referral = models.ForeignKey("self",related_name='referral',null=True, blank=True)
	phone_number = models.CharField(max_length=20, default=' ', null=False, blank=False)
	address = models.CharField(default='', max_length=255, blank=True)
	level_number = models.IntegerField(default=0)
	ref_id = models.CharField(max_length=120,default='',null=True, blank=True)
	ip_address =  models.CharField(default='', max_length=30, blank=True)
	state = models.CharField(max_length=30, default=' ', null=True, blank=True)
	city = models.CharField(max_length=30, default=' ', null=True, blank=True)
	bank_name = models.CharField(max_length=100,default='',null=False,blank=False)
	account_number = models.CharField(max_length=100,default='' ,null=False,blank=False)
	account_name = models.CharField(max_length=100,null=False,blank=False)
	timestamp = models.DateTimeField(auto_now_add=True)
	available = models.BooleanField(default=False)


	def __unicode__(self):
		return str(self.user)

	def __str__(self):
		return str(self.user)

	class Meta:
		unique_together = ("user","ref_id",)



class Payment(models.Model):
	PAYMENT_MEANS_CHOICE = (
		('B','Bank Deposit'),
		('A','ATM Transfer'),
		('M','Mobile Transfer'),
	)
	AMOUNT_CHOICE = (
		('500','N500'),
		('750','N750'),
		('2000','N2,000'),
		('10000','N10,000'),
		('80000','N80,000'),
		('500000','N500,000'),
		)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	payment_to = models.ForeignKey(Profile,related_name='paying',null=True, blank=True)
	amount = models.CharField(max_length=15,choices=AMOUNT_CHOICE,default=0,null=False,blank=False)
	status = models.BooleanField(default=False)
	bank = models.CharField(max_length=30,default='',null=False,blank=False)
	payment_snapshot = models.ImageField(upload_to=payment_upload_location,null=True, blank=True)
	payment_means = models.CharField(max_length=5, choices=PAYMENT_MEANS_CHOICE, default='B', null=False, blank=False)
	date_paid = models.DateField(default = timezone.now)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.user)

	def __str__(self):
		return str(self.user)



# def create_profile(sender, **kwargs):
#     user = kwargs["instance"]
#     if kwargs["created"]:
#         user_profile = Profile(user=user)
#         user_profile.save()
# post_save.connect(create_profile, sender=User)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

# @receiver(post_save, sender=Approval)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Level.objects.create(user=instance.user)

# @receiver(post_save, sender=Approval)
# def save_user_profile(sender, instance, **kwargs):
#     instance.level.save()