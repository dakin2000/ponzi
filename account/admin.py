from django.contrib import admin

from .models import Profile,Payment
# Register your models here.
class ProfileAdmin(admin.ModelAdmin):
	list_display = ["user","direct_referral", "phone_number","level_number", "timestamp","ip_address","ref_id","available"]
	# list_filter = ["first_name"]
	# search_fields = ["first_name", "last_name", "email"]
	class Meta:
		model = Profile

class PaymentAdmin(admin.ModelAdmin):
	list_display = ["user","payment_to","amount","status","bank","payment_means","date_paid"]
	# list_filter = ["first_name"]
	# search_fields = ["first_name", "last_name", "email"]
	class Meta:
		model = Payment

admin.site.register(Profile,ProfileAdmin)
admin.site.register(Payment, PaymentAdmin)