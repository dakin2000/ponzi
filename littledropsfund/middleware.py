from django.utils.deprecation import MiddlewareMixin
from account.models import Profile

class ReferMiddleware(MiddlewareMixin):
	def process_request(self,request):
		ref_id =  request.GET.get('ref','')
		try:
			obj = Profile.objects.get(ref_id=ref_id)
			# print obj
		except:
			obj = None
		if obj:
			request.session['ref'] = obj.id
		print ref_id