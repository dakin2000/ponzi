from .models import Testimony
from django import forms


class TestimonyForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control','row':"5", 'id':"comment"}))
    class Meta:
        model = Testimony
        fields = ['text']