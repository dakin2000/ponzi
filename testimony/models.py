from __future__ import unicode_literals

from django.db import models
from account.models import Profile

# Create your models here.
class Testimony(models.Model):
    author = models.ForeignKey(Profile, related_name='comments')
    text = models.TextField()
    created_date =  models.DateTimeField(auto_now_add=True)
    approved_testimony = models.BooleanField(default=False)

    def approve(self):
        self.approved_testimony = True
        self.save()

    def __str__(self):
        return self.text

    def __unicode__(self):
        return self.text