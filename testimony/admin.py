from django.contrib import admin

# Register your models here.
from .models import Testimony

class TestimonyAdmin(admin.ModelAdmin):
	list_display = ["author","text","created_date","approved_testimony"]
	list_filter = ["author"]
	search_fields = ["author", "created_date", "approved_testimony","text"]
	class Meta:
		model = Testimony

admin.site.register(Testimony,TestimonyAdmin)